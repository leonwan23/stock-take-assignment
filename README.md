# Stock take app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To run

### Using npm
 ```
npm install
```
```
npm start
```
### Using docker compose
```
docker-compose up -d
```
### Using docker image in gitlab container registry
```
docker run -it -p 3000:3000 registry.gitlab.com/leonwan23/stock-take-assignment:latest
```

## To access
### using docker-compose
`stock-take.web.localhost`

### using other 2 methods
`localhost:3000`