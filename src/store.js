import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

import stockReducer from "./stock/stockReducer";

const rootReducer = combineReducers({
  stockReducer
});

export default function configureStore(initialState = {}) {
  const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(thunk))
  );
  return store;
}
