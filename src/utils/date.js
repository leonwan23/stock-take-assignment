import addDays from "date-fns/addDays";
import format from "date-fns/format";
import compareAsc from "date-fns/compareAsc";
import differenceInDays from "date-fns/differenceInDays";

class date {
  addDays(date, numDays) {
    if (!date instanceof Date)
      return console.error("date supplied is not a Date object");
    return addDays(date, numDays);
  }
  format(date, formatString) {
    return format(date, formatString);
  }
  sortAsc(a, b) {
    return compareAsc(a, b);
  }
  differenceInDays([startDate, endDate]) {
    return differenceInDays(new Date(endDate), new Date(startDate));
  }
}

export default new date();
