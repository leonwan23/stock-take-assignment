export const validateQty = qty => {
  if (isNaN(qty) || !Number.isInteger(Number(qty)) || qty <= 0)
    return "Quantity must be a number greater than 0";
  else return "";
};

export const validatePrice = price => {
  if (!/^\d*(?:\.\d{0,2})?$/.test(price) || price <= 0)
    return "Please enter a valid price";
  else return "";
};

export const validateForm = (qty, price) => {
  return !validateQty(qty) && !validatePrice(price);
};
