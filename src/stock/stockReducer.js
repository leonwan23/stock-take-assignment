import { actionTypes } from "./stockActions";

const updateStock = (stockList, stockToUpdate) => {
  return stockList.map(stock => {
    if (stock.id === stockToUpdate.id)
      return Object.assign({}, stock, stockToUpdate);
    else return stock;
  });
};

const findPrice = stock => stock.price * stock.qty;

const updateStats = (stockList, stockToUpdate) => {
  const updatedStock = stockList.find(stock => stock.id === stockToUpdate.id);

  const findDiff = key => stockToUpdate[key] - updatedStock[key];

  const qtyDiff = findDiff("qty");
  const priceDiff = findPrice(stockToUpdate) - findPrice(updatedStock);

  return { qty: qtyDiff, price: priceDiff };
};

export const initialState = {
  stockList: [],
  totalPrice: 0,
  totalQty: 0,
  editingStock: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_STOCK:
      return {
        ...state,
        stockList: [action.payload, ...state.stockList],
        totalPrice: state.totalPrice + findPrice(action.payload),
        totalQty: state.totalQty + action.payload.qty
      };
    case actionTypes.UPDATE_STOCK:
      return {
        ...state,
        stockList: updateStock(state.stockList, action.payload),
        totalPrice:
          state.totalPrice + updateStats(state.stockList, action.payload).price,
        totalQty:
          state.totalQty + updateStats(state.stockList, action.payload).qty
      };
    case actionTypes.DELETE_STOCK:
      return {
        ...state,
        stockList: state.stockList.filter(
          stock => stock.id !== action.payload.id
        ),
        totalPrice: state.totalPrice - findPrice(action.payload),
        totalQty: state.totalQty - action.payload.qty
      };
    case actionTypes.EDIT_STOCK:
      return {
        ...state,
        editingStock: action.payload
      };
    default:
      return state;
  }
};
