import React, { useState, useEffect, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import DatePicker from "react-datepicker";

import { ErrorMsg } from "./StockForm";
import date from "../utils/date";
import { validatePrice, validateQty, validateForm } from "../utils/validation";
import { stockActions } from "./stockActions";

import { ReactComponent as Edit } from "../icons/edit.svg";
import { ReactComponent as Delete } from "../icons/trash.svg";
import "react-datepicker/dist/react-datepicker.css";
import "./stocklist.css";

const formatDate = dte => date.format(dte, "dd MMM, yyyy");
const searchTerm = (str, search) =>
  str.toLowerCase().includes(search.toLowerCase());

function NoResults() {
  return <div className="no-results">No items in stock.</div>;
}

function EditForm({ showForm, toggleShowForm }) {
  const { editingStock } = useSelector(state => state.stockReducer);
  const dispatch = useDispatch();

  const [name, setName] = useState("");
  const [qty, setQty] = useState("");
  const [price, setPrice] = useState("");
  const [date, setDate] = useState(new Date());

  const [qtyError, setQtyError] = useState("");
  const [priceError, setPriceError] = useState("");

  useEffect(() => {
    setName(editingStock?.name || "");
    setQty(editingStock?.qty || "");
    setPrice(editingStock?.price || "");
    setDate(editingStock?.date);
    setQtyError("");
    setPriceError("");
  }, [editingStock, showForm]);

  const updateStock = () => {
    setQtyError(validateQty(qty));
    setPriceError(validatePrice(price));

    if (validateForm(qty, price)) {
      toggleShowForm();
      dispatch(
        stockActions.updateStock({
          ...editingStock,
          name,
          qty: parseInt(qty),
          price: parseFloat(price),
          date
        })
      );
    }
  };

  const isSubmitDisabled = !name || !qty || !price || !date;

  return (
    <Modal show={showForm} onHide={toggleShowForm}>
      <Modal.Header closeButton>
        <Modal.Title>Update item</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group controlId="edit-item" as={Row}>
          <Form.Label column sm="3">
            Item
          </Form.Label>
          <Col sm="8">
            <Form.Control
              type="text"
              size="sm"
              placeholder="Item"
              onChange={e => setName(e.target.value)}
              value={name}
            />
          </Col>
        </Form.Group>

        <Form.Group controlId="edit-qty" as={Row}>
          <Form.Label column sm="3">
            Qty
          </Form.Label>
          <Col sm="8">
            <Form.Control
              type="text"
              size="sm"
              placeholder="Quantity"
              onChange={e => setQty(e.target.value)}
              value={qty}
            />
            {qtyError && <ErrorMsg msg={qtyError} />}
          </Col>
        </Form.Group>
        <Form.Group controlId="edit-price" as={Row}>
          <Form.Label column sm="3">
            Price
          </Form.Label>
          <Col sm="8">
            <Form.Control
              type="text"
              size="sm"
              placeholder="Price"
              onChange={e => setPrice(e.target.value)}
              value={price}
            />
            {priceError && <ErrorMsg msg={priceError} />}
          </Col>
        </Form.Group>

        <Form.Group controlId="edit-date" as={Row}>
          <Form.Label column sm="3">
            Date
          </Form.Label>
          <Col sm="8">
            <DatePicker
              selected={date}
              onChange={setDate}
              id="edit-datepicker"
              dateFormat="dd/MM/yyyy"
            />
          </Col>
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="primary"
          onClick={updateStock}
          size="sm"
          disabled={isSubmitDisabled}
          className="modal-btn"
        >
          Update
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

function StockRow({ stock, toggleEdit }) {
  const dispatch = useDispatch();

  const deleteStock = e => {
    e.preventDefault();
    dispatch(stockActions.deleteStock(stock));
  };

  const editStock = () => {
    toggleEdit();
    dispatch(stockActions.editStock(stock));
  };

  return (
    <div className="list-row">
      <div className="buttons">
        <Delete onClick={deleteStock} title="Delete" />
        <Edit onClick={editStock} title="Edit" />
      </div>
      <div className="list-date">{formatDate(stock.date)}</div>
      <div className="list-name" title={stock.name}>
        {stock.name}
      </div>
      <div className="list-stats">
        <div className="list-price">${stock.price.toFixed(2)}</div>
        <div className="list-qty">
          {stock.qty} <span>in stock</span>
        </div>
      </div>
    </div>
  );
}

export default function StockList() {
  const [showEdit, setShowEdit] = useState(false);
  const [search, setSearch] = useState("");
  const { stockList } = useSelector(state => state.stockReducer);
  const toggleEdit = () => setShowEdit(!showEdit);

  const filteredList = stockList.filter(stock => {
    return (
      searchTerm(stock.name, search) ||
      searchTerm(stock.qty.toString(), search) ||
      searchTerm(stock.price.toString(), search) ||
      searchTerm(formatDate(stock.date), search)
    );
  });

  return (
    <div className="stock-list">
      {stockList.length ? (
        <Fragment>
          <FormControl
            placeholder="Search"
            className="search"
            value={search}
            onChange={e => setSearch(e.target.value)}
          />
          {filteredList.map(stock => (
            <StockRow key={stock.id} stock={stock} toggleEdit={toggleEdit} />
          ))}
        </Fragment>
      ) : (
        <NoResults />
      )}
      <EditForm showForm={showEdit} toggleShowForm={toggleEdit} />
    </div>
  );
}
