import React from "react";
import { render, fireEvent, queryByAttribute } from "../utils/test";

import StockForm from "./StockForm";
import { validateQty } from "../utils/validation";

const getById = queryByAttribute.bind(null, "id");

const setup = () => {
  const utils = render(<StockForm />);
  return {
    ...utils
  };
};

test("Invalid quantity input should display error message", () => {
  const formElement = setup();

  const invalidQty = "test";
  const itemInput = getById(formElement.container, "item");
  const qtyInput = getById(formElement.container, "qty");
  const priceInput = getById(formElement.container, "price");
  const submitButton = getById(formElement.container, "add-stock-btn");

  fireEvent.change(itemInput, { target: { value: "item" } });
  fireEvent.change(qtyInput, { target: { value: invalidQty } });
  fireEvent.change(priceInput, { target: { value: "23" } });
  fireEvent.click(submitButton);

  const errorMsg = formElement.getByText(validateQty(invalidQty));
  expect(errorMsg.textContent).toBe(validateQty(invalidQty));
});
