export const actionTypes = {
  ADD_STOCK: "ADD_STOCK",
  UPDATE_STOCK: "UPDATE_STOCK",
  DELETE_STOCK: "DELETE_STOCK",
  EDIT_STOCK: "EDIT_STOCK"
};

const addStock = stock => {
  return dispatch => {
    dispatch({
      type: actionTypes.ADD_STOCK,
      payload: stock
    });
  };
};

const updateStock = stock => {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_STOCK,
      payload: stock
    });
  };
};

const deleteStock = stock => {
  return dispatch => {
    dispatch({
      type: actionTypes.DELETE_STOCK,
      payload: stock
    });
  };
};

const editStock = stock => {
  return dispatch => {
    dispatch({
      type: actionTypes.EDIT_STOCK,
      payload: stock
    });
  };
};

export const stockActions = {
  addStock,
  updateStock,
  deleteStock,
  editStock
};
