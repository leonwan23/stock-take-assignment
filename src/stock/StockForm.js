import React, { useState } from "react";
import { useDispatch } from "react-redux";
import DatePicker from "react-datepicker";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";

import uuid from "../utils/uuid";
import { stockActions } from "./stockActions";
import { validatePrice, validateQty, validateForm } from "../utils/validation";
import { ReactComponent as Calendar } from "../icons/calendar.svg";

import "react-datepicker/dist/react-datepicker.css";
import "./stockform.css";

export function ErrorMsg({ msg }) {
  return <div className="error">{msg}</div>;
}

export default function StockForm() {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [qty, setQty] = useState("");
  const [price, setPrice] = useState("");
  const [date, setDate] = useState(new Date());

  const [qtyError, setQtyError] = useState("");
  const [priceError, setPriceError] = useState("");

  const resetForm = () => {
    setName("");
    setQty("");
    setPrice("");
    setDate(new Date());
  };

  const addStock = e => {
    e.preventDefault();
    const stock = {
      id: uuid.generate(),
      name,
      qty: parseInt(qty),
      price: parseFloat(price),
      date
    };

    setQtyError(validateQty(qty));
    setPriceError(validatePrice(price));

    if (validateForm(qty, price)) {
      dispatch(stockActions.addStock(stock));
      resetForm();
    }
  };

  const isSubmitDisabled = !name || !qty || !price || !date;

  return (
    <div className="stock-form">
      <Form>
        <Form.Group controlId="item">
          <Col sm="12">
            <Form.Control
              type="text"
              size="sm"
              placeholder="Item"
              onChange={e => setName(e.target.value)}
              value={name}
            />
          </Col>
        </Form.Group>

        <Form.Group controlId="qty">
          <Col sm="12">
            <Form.Control
              type="text"
              size="sm"
              placeholder="Quantity"
              onChange={e => setQty(e.target.value)}
              value={qty}
            />
            {qtyError && <ErrorMsg msg={qtyError} />}
          </Col>
        </Form.Group>
        <Form.Group controlId="price">
          <Col sm="12">
            <Form.Control
              type="text"
              size="sm"
              placeholder="Price"
              onChange={e => setPrice(e.target.value)}
              value={price}
            />
            {priceError && <ErrorMsg msg={priceError} />}
          </Col>
        </Form.Group>

        <Form.Group controlId="date">
          <Col sm="12">
            <label onClick={e => e.preventDefault()}>
              <Calendar id="calendar-icon" />
              <DatePicker
                selected={date}
                onChange={setDate}
                id="datepicker"
                dateFormat="dd/MM/yyyy"
              />
            </label>
          </Col>
        </Form.Group>
        <Col sm="12">
          <Button
            variant="primary"
            type="submit"
            onClick={addStock}
            size="sm"
            className="submit-btn"
            disabled={isSubmitDisabled}
            id="add-stock-btn"
          >
            Add Item
          </Button>
        </Col>
      </Form>
    </div>
  );
}
