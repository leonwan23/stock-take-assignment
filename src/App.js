import React from "react";
import "./App.css";

import { useSelector } from "react-redux";

import StockList from "./stock/StockList";
import StockForm from "./stock/StockForm";
import LineChart from "./visualization/LineChart";
import StatsTotal from "./visualization/StatsTotal";

function App() {
  const { stockList, totalPrice, totalQty } = useSelector(
    state => state.stockReducer
  );
  const itemLabel = stockList.length !== 1 ? "items" : "item";
  return (
    <div className="App">
      <div className="stats-container">
        <div className="stats-component">
          <StatsTotal
            id="total"
            label="total"
            total={totalPrice}
            dollarSign
            decimal
            height={105}
          />
          <StatsTotal id="qty" label="in stock" total={totalQty} height={105} />
          <StatsTotal
            id="items"
            label={itemLabel}
            total={stockList.length}
            height={105}
          />
        </div>
      </div>

      <div className="viz-container">
        <div className="viz-form">
          <StockForm />
          <StockList />
        </div>
        <div className={"viz-charts" + (stockList.length ? "" : " collapsed")}>
          <LineChart
            data={stockList}
            yKey="qty"
            yAxisLabel="Avg qty"
            title="Quantity tracking"
          />
          <LineChart
            data={stockList}
            yKey="price"
            yAxisLabel="Avg price"
            title="Price tracking"
            isPrice
          />
        </div>
      </div>
    </div>
  );
}

export default App;
