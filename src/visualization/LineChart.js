import React, { Component } from "react";
import PropTypes from "prop-types";
import * as d3 from "d3";
import date from "../utils/date";

import "./linechart.css";

const WIDTH = 680;
const HEIGHT = 270;
const GREY = "#808080";

const formatDate = d => date.format(d, "dd-MMM-yy");
const ID_PREFIX = "line-chart-";

export default class LineChart extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        qty: PropTypes.number.isRequired,
        price: PropTypes.number.isRequired,
        date: PropTypes.instanceOf(Date)
      })
    ).isRequired,
    yKey: PropTypes.string.isRequired,
    yAxisLabel: PropTypes.string,
    isPrice: PropTypes.bool
  };
  componentDidMount() {
    this.container = d3.select(`#${ID_PREFIX}${this.props.yKey}`).append("g");
    if (this.props.data.length) {
      this.drawChart();
    }
  }
  componentDidUpdate(prevProps) {
    if (
      this.props.data.length &&
      JSON.stringify(prevProps.data) !== JSON.stringify(this.props.data)
    ) {
      this.drawChart();
    }
  }
  drawChart = () => {
    this.computeData();
    this.calcuateScales();
    this.drawXAxis();
    this.drawYAxis();
    this.drawPoints();
    this.drawLine();
    if (!d3.select("#hover" + this.props.yKey).node()) {
      this.createToolTip();
    }
    this.renderTooltip();
  };

  computeData = () => {
    const { yKey } = this.props;
    const sortByDate = (a, b) => date.sortAsc(a.date, b.date);

    this.data = [];
    const yHash = {};
    const idSet = new Set();
    const aggData = [];

    this.props.data.forEach(d => {
      const formattedDate = formatDate(d.date);
      if (!idSet.has(d.id)) {
        idSet.add(d.id);
        if (yHash[formattedDate]) {
          yHash[formattedDate].total += d[yKey];
          yHash[formattedDate].length += 1;
        } else {
          yHash[formattedDate] = { total: d[yKey], length: 1, name: d.name };
        }
      }
    });
    const total = d3.sum(this.props.data, d => d[yKey]);
    for (let [key, value] of Object.entries(yHash)) {
      const yValue = value.total / value.length;
      aggData.push({
        id: `${key}_${total}_${yValue}_${value.name}`,
        date: new Date(key),
        yValue
      });
    }
    this.data = [...aggData].sort(sortByDate);
  };

  calcuateScales = () => {
    const yExtent = d3.extent(this.data, d => d.yValue);
    this.yScale = d3
      .scaleLinear()
      .range([HEIGHT, 0])
      .domain(yExtent)
      .clamp(true);

    this.xExtent = d3.extent(this.data, d => d.date);
    this.xScale = d3
      .scaleTime()
      .range([0, WIDTH])
      .domain(this.xExtent)
      .nice()
      .clamp(true);
  };

  createToolTip = () => {
    const { yKey, yAxisLabel } = this.props;
    const xPos = -2.4;
    const yPos = 2;
    const tooltipWidth = 115;
    const tooltipHeight = 45;
    const tooltipBorderRadius = 4;

    this.hover = this.container
      .append("g")
      .attr("id", "hover" + yKey)
      .style("display", "none");
    this.hover.append("circle").attr("r", 4).attr("fill", "steelblue");
    this.hover
      .append("rect")
      .attr("class", "tool-tip")
      .attr("height", tooltipHeight)
      .attr("width", tooltipWidth)
      .attr("x", -35)
      .attr("y", 7)
      .attr("rx", tooltipBorderRadius)
      .attr("ry", tooltipBorderRadius);

    this.hover
      .append("text")
      .attr("id", "tooltip-date" + yKey)
      .attr("class", "tooltip-date")
      .attr("text-anchor", "start")
      .attr("dx", xPos + "em")
      .attr("dy", yPos + "em");
    this.hover
      .append("text")
      .attr("text-anchor", "start")
      .attr("dx", xPos + "em")
      .attr("dy", yPos * 2 - 0.2 + "em")
      .attr("class", "tooltip-label")
      .text(yAxisLabel + ":");
    this.hover
      .append("text")
      .attr("id", "tooltip-stats" + yKey)
      .attr("class", "tooltip-stats")
      .attr("text-anchor", "start")
      .attr("x", () => {
        const labelLength = this.hover
          .select(".tooltip-label")
          .node()
          .getComputedTextLength();
        return xPos + labelLength / 12.5 + "em";
      })
      .attr("dy", yPos * 2 - 0.45 + "em");
  };
  renderTooltip = () => {
    const tooltipData = this.data;
    const tooltipElement = this.hover;
    const { isPrice, yKey } = this.props;

    const yScale = this.yScale;
    const xScale = this.xScale;

    const bisectDate = d3.bisector(d => d.date).left;

    if (!d3.select("#overlay" + yKey).node()) {
      this.tooltip = this.container
        .append("rect")
        .attr("id", "overlay" + yKey)
        .attr("fill", "none")
        .attr("pointer-events", "all")
        .attr("width", WIDTH)
        .attr("height", HEIGHT);
    }
    this.tooltip
      .on("mouseover", () => this.hover.style("display", null))
      .on("mouseout", () => this.hover.style("display", "none"))
      .on("mousemove", mousemove);

    function mousemove() {
      let x0 = xScale.invert(d3.mouse(this)[0]);
      let i = bisectDate(tooltipData, x0, 1);
      let d0 = tooltipData[i - 1];
      let d1 = tooltipData[i];
      let d = x0 - d0?.date > d1?.date - x0 ? d1 : d0;

      tooltipElement.attr(
        "transform",
        "translate(" + [xScale(d.date), yScale(d.yValue)] + ")"
      );
      tooltipElement.select("#tooltip-date" + yKey).text(formatDate(d.date));
      tooltipElement
        .select("#tooltip-stats" + yKey)
        .text(isPrice ? d.yValue.toFixed(2) : d.yValue);
    }
  };

  drawXAxis = () => {
    const t = d3.transition().duration(300);
    const xAxis = d3
      .axisBottom(this.xScale)
      .tickFormat(d3.timeFormat("%d-%b-%y"))
      .ticks(d3.timeDay);

    let xaxis = this.container.selectAll("#xaxis" + this.props.yKey).data([1]);
    xaxis.exit().remove();

    const enter = xaxis
      .enter()
      .append("g")
      .attr("class", "axis")
      .attr("id", "xaxis" + this.props.yKey)
      .attr("transform", "translate(" + [0, HEIGHT] + ")");

    xaxis = enter.merge(xaxis);

    xaxis.transition(t).call(xAxis);

    if (date.differenceInDays(this.xExtent) > 10) {
      xaxis
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", "rotate(-45)");
    } else {
      xaxis
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "2.5em")
        .attr("dy", "1em")
        .attr("transform", "rotate(0)");
    }
  };
  drawYAxis = () => {
    const t = d3.transition().duration(300);

    const yAxis = d3.axisLeft(this.yScale);

    let yaxis = this.container.selectAll("#yaxis" + this.props.yKey).data([1]);
    yaxis.exit().remove();

    const enter = yaxis
      .enter()
      .append("g")
      .attr("id", "yaxis" + this.props.yKey)
      .attr("class", "axis");

    // Add axis label:
    yaxis
      .enter()
      .append("text")
      .attr("text-anchor", "end")
      .attr("class", "axis-label")
      .attr("transform", "rotate(-90)")
      .attr("font-size", "12px")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("fill", GREY)
      .text(this.props.yAxisLabel || "Total");

    yaxis = enter.merge(yaxis);
    yaxis.transition(t).call(yAxis);
  };
  drawLine = () => {
    const t = d3.transition().duration(500);
    this.line = this.container
      .selectAll("#line" + this.props.yKey)
      .data([this.data], d => d.id);

    this.line.exit().remove();

    const enter = this.line
      .enter()
      .append("path")
      .attr("id", "line" + this.props.yKey)
      .attr("class", "line")
      .attr("fill", "none")
      .attr("stroke", "steelblue")
      .attr("stroke-width", 1.5)
      .attr("stroke-dasharray", WIDTH * 2 + " " + WIDTH * 2)
      .attr("stroke-dashoffset", WIDTH * 2);
    this.line = enter.merge(this.line);

    d3.select("#line" + this.props.yKey)
      .transition(t)
      .ease(d3.easeLinear)
      .attr(
        "d",
        d3
          .line()
          .curve(d3.curveCatmullRom)
          .x(d => this.xScale(d.date))
          .y(d => this.yScale(d.yValue))
      )
      .attr("stroke-dashoffset", 0);
  };
  drawPoints = () => {
    const t = d3.transition().duration(500);
    this.point = this.container
      .selectAll("#point" + this.props.yKey)
      .data(this.data, (d, i) => d.id + d.yValue + i);

    this.point.exit().remove()

    const enter = this.point
      .enter()
      .append("circle")
      .attr("id", "point" + this.props.yKey)
      .attr("class", "point")
      .attr("fill", "none")
      .attr("stroke", "steelblue")
      .attr("pointer-events", "none")
      .attr("cx", d => this.xScale(d.date))
      .attr("cy", d => this.yScale(d.yValue));
    this.point = enter.merge(this.point);

    this.point.transition(t).attr("r", 2);
  };

  render() {
    return (
      <div className="chart-container card-container">
        {this.props.title && (
          <div className="chart-title">{this.props.title}</div>
        )}
        <svg
          id={ID_PREFIX + this.props.yKey}
          viewBox={`0 0 ${WIDTH} ${HEIGHT}`}
          preserveAspectRatio="xMidYMid meet"
        ></svg>
      </div>
    );
  }
}
