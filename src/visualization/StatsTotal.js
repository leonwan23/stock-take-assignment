import React, { Component } from "react";
import PropTypes from "prop-types";
import * as d3 from "d3";

import "./stats.css";

const WIDTH = 370;
const margin = { bottom: 47 };

const formatId = id => id.replace(/ /g, "_");
const DISPLAY_FONT_SIZE = "36px";

export default class StatsTotal extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    total: PropTypes.number.isRequired,
    dollarSign: PropTypes.bool,
    decimal: PropTypes.bool,
    height: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired
  };
  componentDidMount() {
    this.container = d3.select("#amount-total" + formatId(this.props.id));
    this.styleLabels();
    this.animateTotal();
  }

  componentDidUpdate(prevProps) {
    const { total } = this.props;
    this.styleLabels();
    if (prevProps.total !== total) {
      this.animateTotal(prevProps.total);
    }
  }

  styleLabels = () => {
    this.labels = this.container
      .selectAll("#label" + formatId(this.props.id))
      .data([this.props.label], d => d);

    this.labels.exit().remove();

    const enter = this.labels
      .enter()
      .append("g")
      .attr("id", "label" + formatId(this.props.id))
      .attr(
        "transform",
        "translate(" + [WIDTH / 2, this.props.height - margin.bottom] + ")"
      );

    if (this.props.dollarSign) {
      enter
        .append("text")
        .attr("x", -1)
        .attr("y", 0)
        .attr("text-anchor", "end")
        .style("font-size", DISPLAY_FONT_SIZE)
        .style("font-weight", 500)
        .style("fill", "#fff")
        .text("$");
    }
    enter
      .append("text")
      .attr("x", ".2em")
      .attr("y", "1.5em")
      .attr("text-anchor", "start")
      .style("font-size", "16px")
      .style("font-weight", 600)
      .style("fill", "#aad2f3")
      .text(d => d.toUpperCase());
  };

  animateTotal = (prevTotal = 0) => {
    const { total, decimal } = this.props;
    const t = d3.transition().duration(600);

    this.text = this.container.selectAll("#total" + formatId(this.props.id));

    this.text.transition(t).attr("opacity", 0).remove();

    this.text = this.container
      .append("text")
      .attr(
        "transform",
        "translate(" + [WIDTH / 2, this.props.height - margin.bottom] + ")"
      )
      .attr("id", "total" + formatId(this.props.id))
      .attr("x", 0)
      .attr("y", 0)
      .attr("text-anchor", "start")
      .style("font-size", DISPLAY_FONT_SIZE)
      .style("font-weight", 500)
      .style("fill", "#fff")
      .text(prevTotal);

    this.text.transition(t).tween("text", function () {
      const selection = d3.select(this);
      const start = selection.text();
      const interpolator = d3.interpolateNumber(start, total);
      return t => {
        if (decimal) selection.text(interpolator(t).toFixed(2));
        else selection.text(Math.round(interpolator(t)));
      };
    });
  };

  render() {
    return (
      <div className="stats-total">
        <svg
          id={"amount-total" + formatId(this.props.id)}
          viewBox={`0 0 ${WIDTH} ${this.props.height}`}
          preserveAspectRatio="xMidYMid meet"
        ></svg>
      </div>
    );
  }
}
